\babel@toc {croatian}{}
\contentsline {chapter}{\numberline {1}Uvod}{1}{chapter.1}
\contentsline {chapter}{\numberline {2}Usluge nad internetom}{2}{chapter.2}
\contentsline {section}{\numberline {2.1}TV usluge nad internetom}{2}{section.2.1}
\contentsline {section}{\numberline {2.2}Postoje\IeC {\'c}e video usluge i trendovi}{3}{section.2.2}
\contentsline {chapter}{\numberline {3}Upravljanje digitalnim pravima}{5}{chapter.3}
\contentsline {section}{\numberline {3.1}Komponente DRM sustava}{6}{section.3.1}
\contentsline {section}{\numberline {3.2}Tipi\IeC {\v c}ni model DRM sustava}{7}{section.3.2}
\contentsline {section}{\numberline {3.3}Postoje\IeC {\'c}i komercijalni sustavi}{8}{section.3.3}
\contentsline {chapter}{\numberline {4}Uloga \IeC {\v s}ifriranja u za\IeC {\v s}titi digitalnih prava}{10}{chapter.4}
\contentsline {section}{\numberline {4.1}Simetri\IeC {\v c}no i asimetri\IeC {\v c}no \IeC {\v s}ifriranje}{10}{section.4.1}
\contentsline {section}{\numberline {4.2}\IeC {\v S}ifriranje blokova}{13}{section.4.2}
\contentsline {section}{\numberline {4.3}\IeC {\v S}ifriranje toka}{15}{section.4.3}
\contentsline {section}{\numberline {4.4}AES CTR \IeC {\v s}ifriranje}{17}{section.4.4}
\contentsline {chapter}{\numberline {5}Programsko ostvarenje za\IeC {\v s}tite audio strujanja}{20}{chapter.5}
\contentsline {section}{\numberline {5.1}Za\IeC {\v s}tita metodom parcijalnih klju\IeC {\v c}eva}{21}{section.5.1}
\contentsline {section}{\numberline {5.2}\IeC {\v S}ifriranje i distribucija klju\IeC {\v c}eva}{23}{section.5.2}
\contentsline {section}{\numberline {5.3}De\IeC {\v s}ifriranje na klijentu}{26}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}HTTP djelomi\IeC {\v c}ni zahtjevi}{27}{subsection.5.3.1}
\contentsline {subsection}{\numberline {5.3.2}De\IeC {\v s}ifriranje pro\IeC {\v s}irivanjem AVPlayer komponente}{29}{subsection.5.3.2}
\contentsline {subsection}{\numberline {5.3.3}De\IeC {\v s}ifriranje lokalnim HTTP posrednikom}{30}{subsection.5.3.3}
\contentsline {subsection}{\numberline {5.3.4}Algoritam de\IeC {\v s}ifriranja}{31}{subsection.5.3.4}
\contentsline {chapter}{\numberline {6}Analiza rje\IeC {\v s}enja}{35}{chapter.6}
\contentsline {section}{\numberline {6.1}Kori\IeC {\v s}tene tehnologije}{35}{section.6.1}
\contentsline {subsection}{\numberline {6.1.1}AWS S3 skladi\IeC {\v s}ta}{35}{subsection.6.1.1}
\contentsline {subsection}{\numberline {6.1.2}AWS Lambda funkcije}{36}{subsection.6.1.2}
\contentsline {section}{\numberline {6.2}Ostale tehnologije}{38}{section.6.2}
\contentsline {section}{\numberline {6.3}Analiza mjerljivih svojstava}{39}{section.6.3}
\contentsline {section}{\numberline {6.4}Vektori napada}{41}{section.6.4}
\contentsline {chapter}{\numberline {7}Zaklju\IeC {\v c}ak}{43}{chapter.7}
\contentsline {chapter}{Literatura}{44}{chapter*.30}
