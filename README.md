# DRM - Projekt i Diplomski

Grupni repozitorij za Projekt i Diplomski rad na Fakultetu elektrotehnike i računarstva, Sveučilište u Zagrebu.

Student: Filip Dujmušić

Mentor: doc. dr. sc. Marin Šilić

## Teorijski dio

Ovaj repozitorij sadrži dvije komponente:

* _Projekt_

* _Diplomski_

sa pripadajucćim *LaTeX* izvornim datotekama te izgeneriranom *pdf* verzijom.

**Projekt** se odnosi na pregled idejnog rješenja te detaljni opis implementacije DRM (engl. _digital rights management_) sustava
za zaštitu i strujanje mp3 zapisa.

**Diplomski** rad će osim opisa implementacijskog rješenja sadržati općeniti pregled teme
upravljanja digitalnim pravima te poznatih metoda šifriranja
strujanja (engl. _stream_). Dodatno, bit će prikazana analiza ranjivosti implementiranog rješenja 
zajedno s definicijom pragova prihvatljive ranjivosti.

## Praktični dio 

Praktični dio sastoji se od implementacije sustava za automatsko šifriranje mp3 zapisa i distribuciju ključeva,
te implementacije jednog mobilnog klijenta na iOS platformi, koji je u mogućnosti reproducirati zaključane zapise.
Izvorni kod dostupan je u repozitorijima:

* [AWS šifriranje](https://bitbucket.org/fdujmusic/drm-aws)
* [iOS dešifriranje](https://bitbucket.org/fdujmusic/drm-ios)  
